import React from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity, Alert } from 'react-native';

const TutorialItem = (props) => {
    return (
        <View style={styles.wrapper}>
            <Text style={{ textAlign: 'center' }}>ID: {props.id}</Text>
            <Text style={{ textAlign: 'center' }}>Title: {props.title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        // backgroundColor: 'white',
        // width: '100%',
        // padding: 20,
        // margin: 10,
    }
})

export default TutorialItem;