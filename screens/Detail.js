import React from 'react';
import { View, Text } from 'react-native';

const DetailScreen = ({ route }) => {
    const { TutorialTitle, TutorialDescription, TutorialPushlished } = route.params;
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>
                Title:{JSON.stringify(TutorialTitle)}
            </Text>
            <Text>
                Description: {JSON.stringify(TutorialDescription)}
            </Text>
            <Text>
                Pushlished: {JSON.stringify(TutorialPushlished) ? 'true' : 'false'}
            </Text>
        </View>
    )
}

export default DetailScreen;