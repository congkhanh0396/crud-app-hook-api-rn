import axios from "axios";

export default axios.create({
  baseURL: "https://kan-db.herokuapp.com/api/",
  headers: {
    "Content-type": "application/json"
  }
});

