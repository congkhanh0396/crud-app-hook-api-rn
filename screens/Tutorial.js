import React, { useState, useEffect } from 'react';
import { View, FlatList, Button } from 'react-native';
import TutorialsDataService from '../services/TutorialService';
import TutorialItem from '../components/TutorialItem';
import { TouchableOpacity } from 'react-native-gesture-handler';
function Tutorial({ navigation }) {

    const [tutorials, setTutorials] = useState([]);

    useEffect(() => {
        retrieveTutorials();
    }, []);

    const retrieveTutorials = () => {
        TutorialsDataService.getAll()
            .then(response => {
                setTutorials(response.data);
            })
            .catch(e => {
                console.log(e);
            })
    }

    const deleteTutorial = (id) => {
        TutorialsDataService.remove(id)
            .then(response => {
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            })
    }


    return (
        <FlatList
            data={tutorials}
            keyExtractor={item => item.id.toString()}
            renderItem={renderData =>
                <View key={renderData.item.id} style={{ backgroundColor: 'white', width: '100%', marginVertical: 10, paddingVertical: 10 }}>
                    <TutorialItem
                        id={renderData.item.id}
                        title={renderData.item.title}
                        published={renderData.item.published}
                        description={renderData.item.description}
                    />
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                        <TouchableOpacity style={{ width: 100, alignSelf: 'center', margin: 10 }}>
                            <Button title='Detail' onPress={() => navigation.navigate('Detail', {
                                TutorialTitle: renderData.item.title,
                                TutorialPublished: renderData.item.published,
                                TutorialDescription: renderData.item.description
                            })} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 100, alignSelf: 'center', margin: 10 }}>
                            <Button title='Edit' color='#a89c9b' onPress={() => navigation.navigate('Edit', {
                                 TutorialId: renderData.item.id,
                                 TutorialTitle: renderData.item.title,
                                 TutorialPublished: renderData.item.published,
                                 TutorialDescription: renderData.item.description
                            })} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 100, alignSelf: 'center', margin: 10 }}>
                            <Button title='Delete' color='#f55142' onPress={() => deleteTutorial(renderData.item.id)} />
                        </TouchableOpacity>
                    </View>
                </View>
            }
        />
    )
}



export default Tutorial;