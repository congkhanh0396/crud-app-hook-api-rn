import React from 'react';
import { Alert, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import TutorialScreen from '../screens/Tutorial';
import DetailScreen from '../screens/Detail';
import AddScreen from '../screens/AddTutorial';
import EditScreen from '../screens/Edit';
import Icon from 'react-native-vector-icons/Ionicons';
const Stack = createStackNavigator();

const LoginNavigator = props => {
    return (
        <Stack.Navigator>
            <Stack.Screen component={TutorialScreen} name='Tutorials'
                options={({ navigation }) => ({
                    title: 'My Tutorials',
                    headerRight: () => (
                        <TouchableOpacity style={{ marginHorizontal: 10 }}>
                            <Icon name='create-outline' size={25} onPress={() => navigation.navigate('Add')} />
                        </TouchableOpacity>
                    )
                })}
            />
            <Stack.Screen component={DetailScreen} name='Detail' />
            <Stack.Screen component={AddScreen} name='Add' />
            <Stack.Screen component={EditScreen} name='Edit' />
        </Stack.Navigator>
    )
}


export default LoginNavigator;