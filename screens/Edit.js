import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import TutorialDataService from '../services/TutorialService';
const Edit = ( {route}, navigation ) => {
    const { TutorialId, TutorialTitle, TutorialDescription, TutorialPushlished } = route.params;

    const initialTutorialState = {
        id: null,
        title: "",
        description: "",
        published: false
    };

    const [currentTutorial, setCurrentTutorial] = useState(initialTutorialState);


    const updateTutorial = () => {

        var data = {
            title: currentTutorial.title,
            description: currentTutorial.description
          };
          console.log("data", data);

        TutorialDataService.update(TutorialId, data)
            .then(response => {
                console.log(response.data);
                setCurrentTutorial({
                    id: response.data.id,
                    title: response.data.title,
                    description: response.data.description,
                    published: response.data.published
                })
              
            })
            .catch(e => {
                console.log(e);
            });
    };


    return (
        <View style={styles.Wrapper}>
            <View style={{ margin: 10 }}>
                <Text style={{ textAlign: 'center' }}>Title</Text>
                <TextInput
                    placeholder={TutorialTitle}
                    onChangeText={text => setCurrentTutorial({ ...currentTutorial, title: text })}
                    value={currentTutorial.title}
                />
            </View>
            <View style={{ margin: 10 }}>
                <Text style={{ textAlign: 'center' }}>Description</Text>
                <TextInput
                    placeholder={TutorialDescription}
                    value={currentTutorial.description}
                    onChangeText={text => setCurrentTutorial({ ...currentTutorial, description: text })}
                />
            </View>

            <Button title='Edit Tutorial' onPress={updateTutorial} />
        </View>
    )
}

const styles = StyleSheet.create({
    Wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Edit;