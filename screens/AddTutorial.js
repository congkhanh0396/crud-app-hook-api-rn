import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, Button, Alert } from 'react-native';
import TutorialDataService from '../services/TutorialService';
const AddTutorial = () => {

    const InitialTutorialState = {
        id: null,
        title: "",
        description: "",
        published: false
    }

    const [tutorial, setTutorial] = useState(InitialTutorialState);
    // const [submitted, setSubmitted] = useState(false);


    const saveTutorial = () => {
        // console.log("AAAAAray", tutorial);
        var data = {
            title: tutorial.title,
            description: tutorial.description
        }

        TutorialDataService.create(data)
            .then(response => {
                setTutorial({
                    id: response.data.id,
                    title: response.data.title,
                    description: response.data.description,
                    published: response.data.published
                });
                setSubmitted(true);
                console.log("hello", response.data);
            }).catch(e => {
                console.log("hello", e.response);
            });

    }


    return (
        
        <View style={styles.Wrapper}>
            <View style={{ margin: 10 }}>
                <Text style={{ textAlign: 'center' }}>Title</Text>
                <TextInput
                    id="title"
                    placeholder="Fill the blank with data"
                    name='Title'
                    value={tutorial.title}
                    onChangeText={text => setTutorial({ ...tutorial, title: text })}
                />
            </View>
            <View style={{ margin: 10 }}>
                <Text style={{ textAlign: 'center' }}>Description</Text>
                <TextInput
                    id="description"
                    placeholder="Fill the blank with data"
                    name='Description'
                    value={tutorial.description}
                    onChangeText={text => setTutorial({ ...tutorial, description: text })}
                />
            </View>
            <Button title='Add Tutorial' onPress={saveTutorial} />
        </View>
    )
}

const styles = StyleSheet.create({
    Wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default AddTutorial;